package exam;

import java.util.Scanner;

/**
 * Класс для представления увеличения каждого из чисел на 8
 */

public class Number {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите кол-во чисел:");
        int num = scanner.nextInt(); // переменная которая хранит кол-во повторов цикла
        for (int i = 0; i < num; i++) { // условие цикла
            System.out.println("Введите число:");
            int number = scanner.nextInt(); // число введеное пользователем
            System.out.println(number + " умноженное на 8 = " + (number * 8));
        }
    }
}

